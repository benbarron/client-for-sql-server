# Simple client to connect to remote sql server.
### Description
This is a desktop application that uses the Electron Framework, its' goal is to be simple and only
have the necessary features needed to connect to a remote sql server, run queries and view the results.
This application uses the Ace Text Editor and is a submodule listed in .gitsubmodules.

### Downloading
```
$ git clone https://gitlab.com/benbarron/client-for-sql-server.git
$ cd simple-client-for-sql-server
$ chmod +x setup.sh
$ ./setup.sh
$ npm start
```


### Building A Binary
+ Windows ```$ npm run package-win ```
+ MacOS   ```$ npm run package-mac ```
+ Linux   ```$ npm run package-linux```
