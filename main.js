const { app, BrowserWindow, Menu, MenuItem, globalShortcut  } = require('electron')
const { ipcMain } = require('electron')

const electronLocalShortcut = require('electron-localshortcut');

const { remote } = require('electron')


// ---------------------------------------------------------------------
let win;



// if (process.platform == 'darwin') {
//   const { systemPreferences } = remote

//   const setOSTheme = () => {
//     let theme = systemPreferences.isDarkMode() ? 'dark' : 'light'
//     win.localStorage.os_theme = theme


//     if ('__setTheme' in win) {
//       win.__setTheme()
//     }
//   }

//   systemPreferences.subscribeNotification(
//     'AppleInterfaceThemeChangedNotification',
//     setOSTheme,
//   )

//   setOSTheme()
// }

const createWindow = () => {
    win = new BrowserWindow({
        height: 900,
        width: 1200,
        moveable: true,
        center: true,
        fullscreen: false,
        darkTheme: true,
        webPreferences: {
            nodeIntegration: true
        }
    })

    win.setMenu(null)
    win.loadFile('src/index.html')
    win.on('closed', () =>  win = null)
}


app.on('ready', () => {
  createWindow();

  // file actions
  electronLocalShortcut.register(win, 'Ctrl+N', () => win.webContents.send('action', 'new-file'))
  electronLocalShortcut.register(win, 'Ctrl+O', () => win.webContents.send('action', 'open-file'))
  electronLocalShortcut.register(win, 'Ctrl+S', () => win.webContents.send('action', 'save-file'))


  // quit and reload actions
  electronLocalShortcut.register(win, 'Ctrl+R', () => win.reload())
  electronLocalShortcut.register(win, 'Ctrl+Q', () => app.quit())

  // toggle dev tools
  electronLocalShortcut.register(win, 'Ctrl+D', () => win.toggleDevTools())

  // // Edit menu actions
  // electronLocalShortcut.register(win, 'Ctrl+Z', null) // undo
  // electronLocalShortcut.register(win, 'Ctrl+Y', null) // redo
  // electronLocalShortcut.register(win, 'Ctrl+C', null) // copy
  // electronLocalShortcut.register(win, 'Ctrl+X', null) // cut
  // electronLocalShortcut.register(win, 'Ctrl+V', null) // paste
  // electronLocalShortcut.register(win, 'Ctrl+A', null) // select all

  electronLocalShortcut.register(win, 'Ctrl+Alt+Enter', () => win.webContents.send('action', 'run-all-query'))
  electronLocalShortcut.register(win, 'Ctrl+Enter', () => win.webContents.send('action', 'run-current-query'))
})

app.on('window-all-closed', () => process.platform !== 'darwin' ? app.quit() : null)

app.on('activate', () => win === null ? createWindow() : null);


ipcMain.on('action', (e, m) => {
  switch (m) {
    case 'quit':
      app.quit();
      break;
    case 'reload':
      win.reload();
    case 'toggle-dev-tools':
      win.toggleDevTools();
      break;
    default:
      break;
  }
})







console.log('app started');
