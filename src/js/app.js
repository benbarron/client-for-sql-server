// imports ---------------------------------------------------------------
const fs = require("fs");
const mysql = require('mysql');
const { dialog } = require('electron').remote;
const { ipcRenderer } = require('electron');

// inital settings ---------------------------------------------------------------
var currentMode = "sql";
var currentTheme = "twilight";
var currentTabSize = 2;
var currentFontSize = 20;

var fileLocationOfOpenFile = "";
var saved = true;
var showConnectionMenu = true;
var connectionPayload = {};
var connectionList = [];
let connection;



// initialize text editor ---------------------------------------------------------------
var textEditor = ace.edit("text-editor");

textEditor.session.setMode(`ace/mode/${currentMode}`)
textEditor.setTheme(`ace/theme/${currentTheme}`);
textEditor.setFontSize(currentFontSize);
textEditor.session.setTabSize(currentTabSize);

textEditor.setOptions({
    enableBasicAutocompletion: true,
    enableSnippets: true,
    enableLiveAutocompletion: true
});


// change file saved to false when file is updated
textEditor.session.on('change', () => saved ? saved = false : null);
//     if (saved === true) {
//         saved = false;
//     }
// });

//save file as -----------------------------------------------------------------------------------------------
const saveFileAs = () => {
    dialog.showSaveDialog(filename => {
        if (!filename || filename === undefined) {
            return;
        } else {
            fs.writeFile(filename, textEditor.getValue(), (err, data) => {
                if (err) {
                    alert(err);
                    return;
                }
                changeSyntaxHighlighting(filename);
                fileLocationOfOpenFile = filename;
                document.title = `${fileLocationOfOpenFile} - ${connection ? connectionPayload.user + "@" + connectionPayload.host + ":" + connectionPayload.port : "Not Connected"}`
                saved = true;
            })
        }
    })
}


// save file -----------------------------------------------------------------------------------------------
const saveFile = () => {
    if (fileLocationOfOpenFile === "" || fileLocationOfOpenFile === undefined || !fileLocationOfOpenFile) {
        saveFileAs();
    } else {
        fs.writeFile(fileLocationOfOpenFile, textEditor.getValue(), (err, data) => {
            if (err) {
                alert(err)
                return;
            }

            document.title = `${fileLocationOfOpenFile} - ${connection ? connectionPayload.user + "@" + connectionPayload.host + ":" + connectionPayload.port : "Not Connected"}`
            saved = true;
        })
    }
}


// open file -----------------------------------------------------------------------------------------------
const openFile = () => {
    if (!saved) {
        if (confirm("Would you like to save your file first?")) {
            saveFile()
        }
    }

    dialog.showOpenDialog(filename => {
        if (!filename || filename === undefined) {
            return;
        } else {
            fileLocationOfOpenFile = filename[0];
            fs.readFile(filename[0], "utf-8", (err, data) => {
                if (err) {
                    console.log(err)
                    return;
                } else {
                    changeSyntaxHighlighting(filename[0]);
                    textEditor.setValue(data);
                    document.title = `${filename[0]} - ${connection ? connectionPayload.user + "@" + connectionPayload.host + ":" + connectionPayload.port : "Not Connected"}`
                    saved = true;
                }
            });
        }
    })
}


// new file -----------------------------------------------------------------------------------------------
const newFile = () => {
    if (!saved) {
        if (confirm("Would you like to save this file")) {
            saveFile();
        }
    }

    textEditor.setValue("");
    fileLocationOfOpenFile = "";
    document.title = `Untitled - ${connection ? connectionPayload.user + "@" + connectionPayload.host + ":" + connectionPayload.port : "Not Connected"}`;

    saved = true;
}


// deletes open file -------------------------------------------------------------------------------
const deleteFile = () => {
    var fileToDelete = fileLocationOfOpenFile;

    if(!fileToDelete) {
        alert("You don't have any files currently opened.")
        return;
    }

    if(confirm("You are about to permanently delete the file that is currently opened.")) {
        fs.unlink(fileToDelete, (err) => {
        if(err) {
            alert(err);
            return;
        }
        newFile();
      });
    }
}




// change syntax highlighting when opening a new file
// or saving a file based on the file extension
const changeSyntaxHighlighting = filename => {
    filename = filename.split('.');
    const l = filename.length;
    const extension = filename[l - 1];

    let mode;

    switch(extension) {
        case 'py':
            mode = 'python';
            break;
        case 'sql':
            mode = 'sql';
            break;
        case 'js':
            mode = 'javascript';
            break;
        case 'php':
            mode = 'php';
            break;
        default:
            mode = '';
            break;
    }

    mode ? textEditor.session.setMode(`ace/mode/${mode}`) : null;
}



// makes connection with entered credentials ---------------------------------------------------------------------
const makeConnection = () => {
    const errorMessage = document.getElementById("connect-error-message")
    const host = document.getElementById("host-input").value;
    const port = document.getElementById("port-input").value;
    const user = document.getElementById("user-input").value;
    const password = document.getElementById("password-input").value;
    const database = document.getElementById("database-input").value;

    const errMsg = document.getElementById("connect-error-message")

    if (!host | !port | !user) {
        errMsg.innerHTML = "Host, Port, and User are all required!";
        return;
    }

    connection = mysql.createConnection({ host, port, user, password, database })

    connection.connect(function(err) {
        if (err) {
            errMsg.innerHTML = err;
            setTimeout(() => {
              errMsg.innerHTML = "";
            }, 3000)
            return;
        } else {
            connectionPayload = { host, port, user, password, database }

            document.title = `${fileLocationOfOpenFile ? fileLocationOfOpenFile : "Untitled"} | Connected to: ${connectionPayload.user}@${connectionPayload.host}:${connectionPayload.port}`;
            document.getElementById("make-connection-btn").classList.add("hide")
            document.getElementById("close-connection-btn").classList.remove("hide")
        }
    })
}
document.getElementById("make-connection-btn").addEventListener("click", makeConnection)


// runs query ---------------------------------------------------------------------
const runAllQuery = () => {
    if(!connection) {
        document.querySelector(".results").innerHTML += "<span>You do not have any connections currently established!</span>";
        return;
    }

    var q = textEditor.getValue();

    q = q.split(';');

    for(var i = 0; i < q.length - 1; i++) {
        connection.query(q[i], function(error, results, fields) {
            var res = "";

            if (error) {
                res += `<span style='color:#fff;'>${error}</span><br><br>`;
            } else {
                if(Array.isArray(results)) {
                    var thead = ""
                    var tbody = ""

                    for (i in fields) {
                        thead += `<th scope='col'>${fields[i].name}</th>`
                    }

                    for (i in results) {
                        tbody += `<tr>`
                        for (j in fields) {
                            tbody += `<td>${results[i][`${fields[j].name}`]}</td>`
                        }
                        tbody += `</tr>`
                    }

                    res += `<table><thead>${thead}</thead><tbody>${tbody}</tbody></table><br><br>`;
                } else if (results instanceof Object) {
                    var top = "";
                    var bottom = "";
                    Object.entries(results).forEach(entry => {
                        let key = entry[0];
                        let value = entry[1];
                        top +=`<th>${key}</th>`;
                        bottom += `<td>${value}</td>`;
                    });

                    res += `<table><tbody><tr>${top}</tr><tr>${bottom}</tr></tbody></table><br><br>`;
                }
            }

            var r = document.querySelector('.results');
            var cur = r.innerHTML;
            r.innerHTML = res + cur;
        });
    }

}

const runSelectedQuery = () => {
    if(!connection) {
        document.querySelector(".results").innerHTML += "<span>You do not have any connections currently established!</span>";
        return;
    }

    var q = textEditor.getSelectedText();

    q = q.split(';');

    for(var i = 0; i < q.length - 1; i++) {
        connection.query(q[i], function(error, results, fields) {
            var res = "";

            if (error) {
                res += `<span style='color:#fff;'>${error}</span><br><br>`;
            } else {
                if(Array.isArray(results)) {
                    var thead = ""
                    var tbody = ""

                    for (i in fields) {
                        thead += `<th scope='col'>${fields[i].name}</th>`
                    }

                    for (i in results) {
                        tbody += `<tr>`
                        for (j in fields) {
                            tbody += `<td>${results[i][`${fields[j].name}`]}</td>`
                        }
                        tbody += `</tr>`
                    }

                    res += `<table><thead>${thead}</thead><tbody>${tbody}</tbody></table><br><br>`;
                } else if (results instanceof Object) {
                    var top = "";
                    var bottom = "";
                    Object.entries(results).forEach(entry => {
                        let key = entry[0];
                        let value = entry[1];
                        top +=`<th>${key}</th>`;
                        bottom += `<td>${value}</td>`;
                    });

                    res += `<table><tbody><tr>${top}</tr><tr>${bottom}</tr></tbody></table><br><br>`;
                }
            }

            var r = document.querySelector('.results');
            var cur = r.innerHTML;
            r.innerHTML = res + cur;
        });
    }

}

const runCurrentQuery = () => {
    if(!connection) {
        document.querySelector(".results").innerHTML += "<span>You do not have any connections currently established!</span>";
        return;
    }

    var lines = textEditor.session.doc.getAllLines();
    var cursor = textEditor.getCursorPosition().row;

    var q = ""
    var queries = [];



    var j = 0;
    for(var i = 0; i < lines.length; i++) {
        q += " " + lines[i]
        if(lines[i].includes(";")) {
          queries.push({
            q: q,
            end: i
          });
          q = "";
        }
    }

    for(var i = 0; i < queries.length; i++) {
      if(i === 0) {
        stmt = queries[i]["q"];
      } else {
        if(cursor <= queries[i]["end"] && cursor > queries[i - 1]["end"]) {
          stmt = queries[i]["q"];
        }
      }
    }

    console.log(stmt)

    connection.query(stmt, function(error, results, fields) {
        var res = "";

        if (error) {
            res += `<span style='color:#fff;'>${error}</span><br><br>`;
        } else {
            if(Array.isArray(results)) {
                var thead = ""
                var tbody = ""

                for (i in fields) {
                    thead += `<th scope='col'>${fields[i].name}</th>`
                }

                for (i in results) {
                    tbody += `<tr>`
                    for (j in fields) {
                        tbody += `<td>${results[i][`${fields[j].name}`]}</td>`
                    }
                    tbody += `</tr>`
                }

                res += `<table><thead>${thead}</thead><tbody>${tbody}</tbody></table><br><br>`;
            } else if (results instanceof Object) {
                var top = "";
                var bottom = "";
                Object.entries(results).forEach(entry => {
                    let key = entry[0];
                    let value = entry[1];
                    top +=`<th>${key}</th>`;
                    bottom += `<td>${value}</td>`;
                });

                res += `<table><tbody><tr>${top}</tr><tr>${bottom}</tr></tbody></table><br><br>`;
            }
        }

        var r = document.querySelector('.results');
        var cur = r.innerHTML;
        r.innerHTML = res + cur;
    });
}





// ends current connection ---------------------------------------------------------------------
const endConnection = () => {
    if (connection) {
        connection.end();

        connectionPayload = {};

        document.title = `${fileLocationOfOpenFile ? fileLocationOfOpenFile : "Untitled"} | Not Connected`;

        document.getElementById("make-connection-btn").classList.remove("hide")
        document.getElementById("close-connection-btn").classList.add("hide")

        return;
    }
}
document.getElementById("close-connection-btn").addEventListener("click", endConnection)


// clears results section ---------------------------------------------------------------------
const clearResults = () => {
    document.querySelector('.results').innerHTML = "";
}



const quitApp = () => ipcRenderer.send('action', 'quit');

const reloadApp = () => ipcRenderer.send('action', 'reload');


// handler for file actions
document.querySelectorAll(".file-action").forEach(action => {
  action.addEventListener("click", e => {
    switch(e.target.parentNode.id) {
        case 'new-file':
            newFile();
            break;
        case 'save-file':
            saveFile();
            break;
        case 'open-file':
            openFile();
            break;
        case 'delete-file':
            deleteFile();
            break;
        case 'reload':
            reloadApp();
            break;
        case 'quit':
            quitApp();
            break;
        default:
            break;
    }
  })
});

// handler for edit actions
document.querySelectorAll(".edit-action").forEach(action => {
  action.addEventListener("click", e => {
    ipcRenderer.send('action', e.target.parentNode.id);
  })
})


// handler for connection actions
document.querySelectorAll(".connection-action").forEach(action => {
  action.addEventListener("click", e => {
    console.log(e.target.parentNode.id)
    switch(e.target.parentNode.id) {
        case 'run-all-query':
            runAllQuery();
            break;
        case 'run-current-query':
            runCurrentQuery();
            break;
        case 'run-selected-query':
            runSelectedQuery();
            break;
        case 'close-connection':
            endConnection();
            break;
        case 'clear-results':
            clearResults();
            break;
        default:
            break;
    }
  })
});

// hanler for changing of syntax highlighting
const manualSetSyntaxHighlighting = mode => {
    textEditor.session.setMode(`ace/mode/${mode}`)
    currentMode = mode;
}

document.querySelectorAll(".change-syntax").forEach(syntax => {
  syntax.addEventListener("click", e => {
    manualSetSyntaxHighlighting(e.target.parentNode.id);
  })
})

// handler for changes in font size
const changeFontSize = size => {
    var value = textEditor.getValue();

    textEditor = ace.edit("text-editor");
    textEditor.setValue(value);
    textEditor.setTheme(`ace/theme/${currentTheme}`);
    textEditor.setFontSize(parseInt(size));
    textEditor.session.setTabSize(currentTabSize);
    textEditor.session.setMode(`ace/mode/${currentMode}`);

    currentFontSize = parseInt(size);
}

document.querySelectorAll(".change-font-size").forEach(fontSize => {
  fontSize.addEventListener("click", e => {
    changeFontSize(e.target.parentNode.id);
  })
})


// hadler for changes in tab size
const changeTabSize = size => {
    textEditor.session.setTabSize(parseInt(size));
    currentTabSize = parseInt(size);
}

document.querySelectorAll(".change-tab-size").forEach(tabSize => {
  tabSize.addEventListener("click", e => {
    changeTabSize(e.target.parentNode.id);
  })
})




// handler for changing of theme
const changeTheme = theme => {
    var value = textEditor.getValue();

    textEditor = ace.edit("text-editor")
    textEditor.setTheme(`ace/theme/${theme}`);
    textEditor.setValue(value);
    textEditor.session.setMode(`ace/mode/${currentMode}`);
    textEditor.session.setTabSize(currentTabSize);
    textEditor.setFontSize(currentFontSize);

    currentTheme = theme;
}

document.querySelectorAll(".change-theme").forEach(theme => {
  theme.addEventListener("click", e => {
    changeTheme(e.target.parentNode.id);
  })
})


// toggle connection menu ---------------------------------------------------------------------
const toggleConnectionMenu = () => {
    var re = document.querySelector('.results');
    var te = document.querySelector('#text-editor');
    var cs = document.querySelector('#connection-settings');

    if(showConnectionMenu) {
        re.classList.add('expand');
        te.classList.add('expand');
        cs.classList.add('slide');
        showConnectionMenu = false;
    } else {
        re.classList.remove('expand');
        te.classList.remove('expand');
        cs.classList.remove('slide');
        showConnectionMenu = true;
    }
}
document.querySelector('.connection-settings-toggle-btn').addEventListener("click", toggleConnectionMenu)

document.getElementById("dev-tools-toggler").addEventListener("click", e => ipcRenderer.send('action', 'toggle-dev-tools'))


// ipc renderer for key board shortcuts coming from main process
ipcRenderer.on('action', (e, m) => {

  switch (m) {
    case 'new-file':
        newFile();
        break;
    case 'open-file':
        openFile();
        break;
    case 'save-file':
        saveFile();
        break;
    case 'run-all-query':
        runAllQuery();
        break;
    case 'run-current-query':
        runCurrentQuery();
        break;
    case 'run-selected-query':
        runSelectedQuery();
        break;
    default:
        break;
  }
})

// listens for loading of content
window.addEventListener('DOMContentLoaded', (event) => {
    newFile();
});
