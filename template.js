var template = [{
        label: 'File',
        submenu: [
            {
                label: "New File",
                accelerator: 'Ctrl+n',
                click: () => win.webContents.send('file-action', 'new-file')
            },
            {
                label: "Open File",
                accelerator: 'Ctrl+o',
                click: () => win.webContents.send('file-action', 'open-file')
            },
            {
                label: "Save File",
                accelerator: 'Ctrl+s',
                click: () => win.webContents.send('file-action', 'save-file')
            },
            {
                label: "Save File As",
                accelerator: '',
                click: () => win.webContents.send('file-action', 'save-file-as')
            },
            {
                label: "Delete File",
                click: () => win.webContents.send('file-action', 'delete-file')
            },
            {
                role: "separator"
            },
            {
              role: "reload"
            },
            {
                role: "quit"
            }
        ]
    },
    {
        label: "Edit",
        submenu: [
            { role: 'undo' },
            { role: 'redo' },
            { role: 'cut' },
            { role: 'paste' },
            { role: 'delete' },
            { role: 'selectall' }
        ]
    },
    {
        label: "Options",
        submenu: [
            {
                label: 'Editor Themes',
                submenu: [
                    {
                        label: "IPlastic",
                        click: () => win.webContents.send('change-theme', 'iplastic')
                    },
                    {
                        label: "Text Mate",
                        click: () => win.webContents.send('change-theme', 'textmate')
                    },
                    {
                        label: "Twiligt",
                        click: () => win.webContents.send('change-theme', 'twilight')
                    },
                    {
                        label: "Terminal",
                        click: () => win.webContents.send('change-theme', 'terminal')
                    },
                    {
                        label: "Tomorrow Night",
                        click: () => win.webContents.send('change-theme', 'tomorrow_night')
                    },
                    {
                        label: "Pastel On Dark",
                        click: () => win.webContents.send('change-theme', 'pastel_on_dark')
                    },
                    {
                        label: "Dracula",
                        click: () => win.webContents.send('change-theme', 'dracula')
                    }
                ]
            },
            {
                label: "Syntax Highlighting",
                submenu: [
                    {
                        label: "SQL",
                        click: () => win.webContents.send('change-highlights', 'sql')
                    },
                    {
                        label: "Python",
                        click: () => win.webContents.send('change-highlights', 'python')
                    },
                    {
                        label: "Javascript",
                        click: () => win.webContents.send('change-highlights', 'javascript')
                    }
                ]
            },
            {
                label: "Font Size",
                submenu: [
                    {
                        label: "8pt",
                        click: () => win.webContents.send('change-font-size', 8)
                    },
                    {
                        label: "10pt",
                        click: () => win.webContents.send('change-font-size', 10)
                    },
                    {
                        label: "12pt",
                        click: () => win.webContents.send('change-font-size', 12)
                    },
                    {
                        label: "14pt",
                        click: () => win.webContents.send('change-font-size', 14)
                    },
                    {
                        label: "16pt",
                        click: () => win.webContents.send('change-font-size', 16)
                    },
                    {
                        label: "18pt",
                        click: () => win.webContents.send('change-font-size', 18)
                    },
                    {
                        label: "20pt",
                        click: () => win.webContents.send('change-font-size', 20)
                    },
                    {
                        label: "22pt",
                        click: () => win.webContents.send('change-font-size', 22)
                    },
                    {
                        label: "24pt",
                        click: () => win.webContents.send('change-font-size', 24)
                    },
                ]
            },
            {
                label: "Indentation Size",
                submenu: [
                    {
                        label: "1",
                        click: () => win.webContents.send('change-tab-size', 1)
                    },
                    {
                        label: "2",
                        click:  () => win.webContents.send('change-tab-size', 2)
                    },
                    {
                        label: "3",
                        click:  () => win.webContents.send('change-tab-size', 3)
                    },
                    {
                        label: "4",
                        click:  () => win.webContents.send('change-tab-size', 4)
                    },
                    {
                        label: "5",
                        click:  () => win.webContents.send('change-tab-size', 5)
                    },
                    {
                        label: "6",
                        click:  () => win.webContents.send('change-tab-size', 6)
                    },
                    {
                        label: "7",
                        click:  () => win.webContents.send('change-tab-size', 7)
                    },
                    {
                        label: "8",
                        click:  () => win.webContents.send('change-tab-size', 8)
                    }
                ]
            },
            {
                label: "Toggle Dev Tools",
                accelerator: 'Ctrl+d',
                click: () => {
                    if (devTools) {
                        win.webContents.closeDevTools();
                        devTools = false;
                    } else {
                        win.webContents.openDevTools()
                        devTools = true;
                    }

                }
            }
        ]
    },
    {
        label: "Connection",
        submenu: [
            {
                label: "Run Entire Script",
                accelerator: "Ctrl+Alt+Enter",
                click: () => win.webContents.send('connection-actions', 'run-all-query')
            },
            {
                label: "Run Highlighted Statement",
                accelerator: "Ctrl+Enter",
                click: () => win.webContents.send('connection-actions', 'run-single-query')
            },
            {
                label: "Clear Results",
                click: () => win.webContents.send('connection-actions', 'clear-results')
            },
            {
                label: "End Connection",
                click: () => win.webContents.send('connection-actions', 'close-connection')
            },

        ]
    }
];



    // application menu
    // Menu.setApplicationMenu(Menu.buildFromTemplate(template));
